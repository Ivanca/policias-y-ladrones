﻿using UnityEditor;
using UnityEngine;
[CustomEditor(typeof(Rigidbody))]
public class RigidbodyEditor : Editor
{
    void OnSceneGUI()
    {
        Rigidbody rb = target as Rigidbody;
        Handles.color = Color.red;
        Handles.SphereCap(1, rb.transform.TransformPoint(rb.centerOfMass), rb.rotation, 1f);
    }

    void OnEnable()
    {
        
        if (Application.isPlaying)
        {
            Debug.Log(target);
            Rigidbody rb = target as Rigidbody;
            if (rb.transform.gameObject.GetComponent<ShowGizmo>() == null)
            {
                rb.transform.gameObject.AddComponent<ShowGizmo>();
            }
        }
        //rb.transform.gameObject.AddComponent<ShowGizmo>();
    }

    public override void OnInspectorGUI()
    {
        Debug.Log("all rgith");
        GUI.skin = EditorGUIUtility.GetBuiltinSkin(UnityEditor.EditorSkin.Inspector);
        DrawDefaultInspector();
    }
}

public class ShowGizmo : MonoBehaviour
{
    void OnDrawGizmos ()
    {
        Rigidbody rb = transform.GetComponent<Rigidbody>();
        Matrix4x4 oldGizmosMatrix = Gizmos.matrix;
        Gizmos.matrix = Matrix4x4.TRS(rb.transform.TransformPoint(rb.centerOfMass), rb.rotation, Vector3.one * 2);
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(Vector3.zero, 2);
        Gizmos.matrix = oldGizmosMatrix;
    }
}